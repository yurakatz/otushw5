﻿using System;
using System.Diagnostics;
using Jil;
using Newtonsoft.Json;

namespace otushw5
{
    internal class Program
    {
        private static void Main()
        {
            var testedData = new F {I1 = 1, I2 = 2, I3 = 3, I4 = 4, I5 = 5};

            //test custom CsvSerializer
            var sw = Stopwatch.StartNew();
            for (var index = 0; index < 100000; index++) CsvSerializer<F>.ToCsv(testedData);
            sw.Stop();

            Console.WriteLine($"CSV Serialization Test ; Execution time :{sw.ElapsedMilliseconds} ms");

            var result = CsvSerializer<F>.ToCsv(testedData);


            sw.Restart();
            Console.WriteLine($"Result :{result}");
            sw.Stop();


            Console.WriteLine($"Console WriteTime ; Execution time :{sw.ElapsedTicks} Ticks");


            sw.Restart();
            for (var index = 0; index < 100000; index++) CsvSerializer<F>.Deserialize(result);
            sw.Stop();
            Console.WriteLine($"CSV Deserialize Test    ; Execution time :{sw.ElapsedMilliseconds} ms");

            //test   JsonConvert Serializer
            sw.Restart();
            for (var index = 0; index < 100000; index++) JsonConvert.SerializeObject(testedData);
            sw.Stop();
            Console.WriteLine($"Newtonsoft Serialize Test    ; Execution time :{sw.ElapsedMilliseconds} ms");

            result = JsonConvert.SerializeObject(testedData);

            sw.Restart();
            for (var index = 0; index < 100000; index++) JsonConvert.DeserializeObject(result);
            sw.Stop();
            Console.WriteLine($"Newtonsoft Deserialize Test    ; Execution time :{sw.ElapsedMilliseconds} ms");

            //test   Jil Serializer
            sw.Restart();
            for (var index = 0; index < 100000; index++) JSON.Serialize(testedData);
            sw.Stop();
            Console.WriteLine($"Jil Serialize Test    ; Execution time :{sw.ElapsedMilliseconds} ms");

            result = JSON.Serialize(testedData);

            sw.Restart();
            for (var index = 0; index < 100000; index++) JSON.Deserialize<F>(result);
            sw.Stop();
            Console.WriteLine($"Jil Deserialize Test    ; Execution time :{sw.ElapsedMilliseconds} ms");


            Console.ReadLine();
        }
    }
}