﻿using System;
using System.Linq;
using System.Reflection;

namespace otushw5
{
    public static class CsvSerializer<T> where T : class, new()
    {
        public static string ToCsv(T data)
        {
            return string.Join(",",
                typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance)
                    .Select(p => $@"{p.Name},{p.GetValue(data)}").ToList());
        }

        public static T Deserialize(string data)
        {
            var retrieval = new T();

            var propertySplit = data.Split(",");
            var properties = retrieval.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public);

            if (propertySplit.Length % 2 != 0)
                throw new ArgumentException("Illegal format");

            for (var i = 0; i < propertySplit.Length; i += 2)
            {
                var p = properties.SingleOrDefault(x => x.Name == propertySplit[i]);

                if (p == null) continue;
                var value = Convert.ChangeType(propertySplit[i + 1], p.PropertyType);
                p.SetValue(retrieval, value);
            }

            return retrieval;
        }
    }
}